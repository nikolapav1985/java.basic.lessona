/**
*
* public class BasicModB
*
* a basic example in Java
*
* just print a message
*
* ----- compile -----
*
* javac -g BasicModB.java
*
* ----- run -----
*
* java BasicModB 
*
* ----- output example -----
*
* Hello everyone!
*
*/
public class BasicModB{ // explain class BasicModB
    // public - publicaly available class, can be used by users or some other process
    // protected - class available only inside a library (not outside)
    // private - class available only at place of definition (not in any other place)
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args){ // main method
        // String []args, array of strings, array of array of characters
        // basic string example 123abcde
        // point of entry into the program
        // program starts running after this line
        System.out.println("Hello everyone!");
    }
}
