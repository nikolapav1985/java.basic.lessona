/**
*
* public class Name
*
* a basic example of branching (using switch) in Java
*
* ----- compile -----
*
* javac -g Name.java
*
* ----- run -----
*
* java Name Steve 2
*
* ----- output example -----
*
* Hello Steve. First letter of your name is S.
*
*/
public class Name{ 
    public static void main(String []args){ // main method
        String name; // your name as sequence of characters or array of characters
        int option;
        int len; // number of characters in your name
        String reverse; // your name reversed
        if(args.length != 2){ // need couple of command line arguments
            // args[0] name
            // args[1] option
            System.exit(1); // no parameters, just exit this program
        }
        name=args[0]; // argument is first element in the command line
        option=Integer.parseInt(args[1]); // get option, integer value
        len=name.length(); // get name length
        reverse=(new StringBuilder(name)).reverse().toString(); // reverse your name
        switch(option){
            case 1: 
                System.out.println("Hello "+name+". Number of characters in your name is "+len+".");
                break;
            case 2: 
                System.out.println("Hello "+name+". First letter of your name is "+name.charAt(0)+".");
                break;
            case 3: 
                System.out.println("Hello "+name+". Your name reversed is "+reverse+".");
                break;
            default:
                System.out.println("Hello "+name+". This option is NOT valid.");
                break;
        }
    }
}
