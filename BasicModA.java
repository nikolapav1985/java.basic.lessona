/**
*
* public class BasicModA
*
* a basic example in Java
*
* read your name and print number of characters in your name
*
* ----- compile -----
*
* javac -g BasicModA.java
*
* ----- run -----
*
* java BasicModA John Doe
*
* ----- output example -----
*
* Hello John. Number of characters in your name is 4.
* Hello Doe. Number of characters in your last name is 3.
*
*/
public class BasicModA{ // explain class Basic
    // public - publicaly available class, can be used by users or some other process
    // protected - class available only inside a library (not outside)
    // private - class available only at place of definition (not in any other place)
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args){ // main method
        // String []args, array of strings, array of array of characters
        // basic string example 123abcde
        // point of entry into the program
        // program starts running after this line
        String name; // your name as sequence of characters or array of characters
        String lastname; // your last name as sequence of characters or array of characters
        int len; // number of characters in your name
        int lastlen; // number of characters in your last name
        if(args.length != 2){ // need couple of command line arguments
            System.exit(1); // no parameter, just exit this program
        }
        name=args[0]; // argument is first element in the command line
            // args[0] args[1] args[2] ...
            // a b c d e f g h 123abcd ...
        lastname=args[1]; // argument is second element in the command line
        len=name.length(); // get name length
        lastlen=lastname.length(); // number of characters of last name
        System.out.println("Hello "+name+". Number of characters in your name is "+len+".");
        System.out.println("Hello "+lastname+". Number of characters in your last name is "+lastlen+".");
    }
}
