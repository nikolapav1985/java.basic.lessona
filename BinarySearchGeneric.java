import java.util.*;
/**
*
* public class BinarySearchGeneric
*
* do a binary search on array of any type
*
* if success, return index of matching element
* if fail, return -1 (index and element not in array)
*
* number of steps to do search log(2) n (n - length of array)
* that is, keep halving array during search
*
*/
public class BinarySearchGeneric<T>{
    public int doSearch(T item, int lo, int hi, Comparator cmp, T[] arr){ // doSearch method
        // item - item to look for
        // lo - least index in array
        // hi - highest index in array
        // cmp - comparator 
        // arr - array of items
        // doSearch on sorted array
        int mid;
        for(;lo <= hi;){ // keep running until items available
            mid=(lo+hi)/2;
            if(cmp.compare(arr[mid],item)==0){
                return mid; // element found, return index, can also use break, can also use goto
            }
            if(cmp.compare(item,arr[mid])<0){
                hi=mid;
                continue; // item is less, look at left side
            }
            if(cmp.compare(item,arr[mid])>0){
                lo=mid; // item is higher, look at right side
            }
        }
        return -1; // item not found, index not in array
    }
}
