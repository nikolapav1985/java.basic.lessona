/**
*
* public class BinarySearch
*
* do a binary search on array of integers
*
* if success, return index of matching element
* if fail, return -1 (index and element not in array)
*
* number of steps to do search log(2) n (n - length of array)
* that is, keep halving array during search
*
*/
public class BinarySearch{
    public int doSearch(int item, int lo, int hi, int[] arr){ // doSearch method
        // item - item to look for
        // lo - least index in array
        // hi - highest index in array
        // arr - array of items
        // doSearch on sorted array
        int mid;
        for(;lo <= hi;){ // keep running until items available
            mid=(lo+hi)/2;
            if(arr[mid]==item){
                return mid; // element found, return index, can also use break, can also use goto
            }
            if(item<arr[mid]){
                hi=mid;
                continue; // item is less, look at left side
            }
            if(item>arr[mid]){
                lo=mid; // item is higher, look at right side
            }
        }
        return -1; // item not found, index not in array
    }
}
