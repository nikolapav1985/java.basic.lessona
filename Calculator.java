/**
*
* public class Calculator
*
* a basic example of number types and operations in Java
*
* ----- compile -----
*
* javac -g Calculator.java
*
* ----- run -----
*
* java Calculator 5 3 1
*
* ----- output example -----
*
* The sum of 5 and 3 is 8.
*
*/
public class Calculator{ // explain class Calculator 
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args){ // main method
        // point of entry into the program
        // program starts running after this line
        int a,b,option;
        if(args.length != 3){ // need three command line arguments
            System.exit(1); // no arguments, just exit this program
        }
        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        option=Integer.parseInt(args[2]);
        switch(option){ // TODO case 3 division, case 4 remainder
            case 1:
                System.out.println(String.format("The sum of %d and %d is %d.",a,b,a+b));
                break;
            case 2:
                System.out.println(String.format("The difference of %d and %d is %d.",a,b,a-b));
                break;
            default: 
                break;
        }
    }
}
