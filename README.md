Basic lesson A
--------------

- Basic.java (basic Java example, check comments for details)
- Name.java (branching Java example using switch, check comments for details)
- BinarySearch.java (looping example in Java, check comments for details)
- BinarySearchGeneric.java (looping example in Java generic, check comments for details)
- BinarySearchTest.java (a test program for binary search, check comments for details)
- BinarySearchGenericTest.java (a test program for binary search generic, check comments for details)
- CompInt.java (a comparator for integers in Java, check comments for details)
- Calculator.java (number types in Java example, check comments for details)
- CalculatorFile.java (number types in Java example, read from file, check comments for details)
- CalculatorStdin.java (number types in Java example, read from file, check comments for details)
- BasicModA.java (basic Java example, couple of command line arguments, check comments for details)
- BasicModB.java (basic Java example, print a message, check comments for details)
- BasicModC.java (basic Java example, print a message, check comments for details)

Topic covered (branching)
-------------------------

- if else (if condition true do this part, if not do other part)
- switch (check for available and default options)

Topic covered (looping)
-----------------------

- for loop (basic conditions and body of the loop)
- while loop (basic conditions and body of the loop)

Topic covered (number types and operations)
-------------------------------------------

- number types like integer, double, float
- basic operations addition, subtraction, division, remainder (+ - / %)
