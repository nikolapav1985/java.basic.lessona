import java.util.*;

/**
*
* public class CalculatorStdin
*
* a basic example of number types and operations in Java, read from standard input
*
* ----- compile -----
*
* javac -g CalculatorStdin.java
*
* ----- run -----
*
* java CalculatorStdin < filename
*
* ----- output example -----
*
* The sum of 5 and 3 is 8.
*
*/
public class CalculatorStdin{ // explain class CalculatorStdin 
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args){ // main method
        // point of entry into the program
        // program starts running after this line
        int a,b,option;
        Scanner scan=new Scanner(System.in);
        a=Integer.parseInt(scan.nextLine());
        b=Integer.parseInt(scan.nextLine());
        option=Integer.parseInt(scan.nextLine());
        switch(option){ // TODO case 3 division, case 4 remainder
            case 1:
                System.out.println(String.format("The sum of %d and %d is %d.",a,b,a+b));
                break;
            case 2:
                System.out.println(String.format("The difference of %d and %d is %d.",a,b,a-b));
                break;
            default: 
                break;
        }
    }
}
