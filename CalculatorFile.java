import java.io.*;
import java.util.*;

/**
*
* public class CalculatorFile
*
* a basic example of number types and operations in Java, read from file
*
* ----- compile -----
*
* javac -g CalculatorFile.java
*
* ----- run -----
*
* java CalculatorFile filename
*
* ----- output example -----
*
* The sum of 5 and 3 is 8.
*
*/
public class CalculatorFile{ // explain class Calculator 
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args) throws FileNotFoundException{ // main method
        // point of entry into the program
        // program starts running after this line
        int a,b,option;
        String filename;
        File file;
        Scanner scan;
        if(args.length != 1){ // need one command line argument
            System.exit(1); // no arguments, just exit this program
        }
        filename=args[0];
        file=new File(filename); // open file
        scan=new Scanner(file); // use to read file
        a=Integer.parseInt(scan.next());
        b=Integer.parseInt(scan.next());
        option=Integer.parseInt(scan.next());
        switch(option){ // TODO case 3 division, case 4 remainder
            case 1:
                System.out.println(String.format("The sum of %d and %d is %d.",a,b,a+b));
                break;
            case 2:
                System.out.println(String.format("The difference of %d and %d is %d.",a,b,a-b));
                break;
            default: 
                break;
        }
    }
}
