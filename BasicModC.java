public class BasicModC{
    public static void main(String []args){
        System.out.println("Hello everyone!");
        System.out.print("Hello everyone! Line 4.");
        System.out.print("Hello everyone! Line 5.");
    }
}

/**
public class AClass{
    public static int a; // variable a
    public static int b(){ // method b
    }
}
public class BClass{
}
public class Machine{
}
Aclass a=new AClass();
Aclass aa=new AClass();
Bclass b=new BClass();
Bclass bb=new BClass();
Machine desktop=new Machine();
Machine laptop=new Machine();
Machine rpi=new Machine();
Machine arduino=new Machine();
*/
