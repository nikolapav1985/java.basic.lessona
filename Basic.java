/**
*
* public class Basic
*
* a basic example in Java
*
* read your name and print number of characters in your name
*
* ----- compile -----
*
* javac -g Basic.java
*
* ----- run -----
*
* java Basic Steve
*
* ----- output example -----
*
* Hello Steve. Number of characters in your name is 5.
*
*/
public class Basic{ // explain class Basic
    // public - publicaly available class, can be used by users or some other process
    // protected - class available only inside a library (not outside)
    // private - class available only at place of definition (not in any other place)
    // in simple terms class is a set
    // in this case only one instance of the class (only one object of this class)
    // in this case class instance is used to run a program
    public static void main(String []args){ // main method
        // String []args, array of strings, array of array of characters
        // basic string example 123abcde
        // point of entry into the program
        // program starts running after this line
        String name; // your name as sequence of characters or array of characters
        int len; // number of characters in your name
        if(args.length != 1){ // need one command line argument
            System.exit(1); // no parameter, just exit this program
        }
        name=args[0]; // argument is first element in the command line
            // args[0] args[1] args[2] ...
            // a b c d e f g h 123abcd ...
        len=name.length(); // get name length
        System.out.println("Hello "+name+". Number of characters in your name is "+len+".");
    }
}
